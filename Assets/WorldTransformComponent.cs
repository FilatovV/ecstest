﻿using UnityEngine;
using Unity.Entities;
using System;

[Serializable]
public struct WorldTransformData : IComponentData
{
    public Vector2 position;
    public float rotateAngle;
}

public class WorldTransformComponent : ComponentDataProxy<WorldTransformData> { }
