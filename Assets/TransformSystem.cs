﻿using UnityEngine;
using Unity.Entities;


[UpdateBefore(typeof(WorldTransformSystem))]
public class TransformSystem : ComponentSystem {

    protected override void OnUpdate() {
        Entities.ForEach((Transform transform, ref TransformData transformData) => {
            var currentPosition = new Vector2(transform.localPosition.x, transform.localPosition.y);
            if (transformData.position != currentPosition) {
                transform.localPosition = new Vector3(transformData.position.x, transformData.position.y, transform.localPosition.z);
            }

            var currentRotation = transform.localEulerAngles.z;
            if (transformData.rotateAngle != currentRotation) {
                transform.localRotation = Quaternion.Euler(transform.localEulerAngles.x, transform.localEulerAngles.y, transformData.rotateAngle);
            }
        });
    }
}
