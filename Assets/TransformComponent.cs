﻿using UnityEngine;
using Unity.Entities;
using System;

[Serializable]
public struct TransformData : IComponentData {
    public Vector2 position;
    public float rotateAngle;
}

public class TransformComponent : ComponentDataProxy<TransformData> { }