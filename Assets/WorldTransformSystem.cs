﻿using UnityEngine;
using Unity.Entities;


public class WorldTransformSystem : ComponentSystem
{

    protected override void OnUpdate()
    {
        Entities.ForEach((Transform transform, ref WorldTransformData transformData) => {
            var currentPosition = new Vector2(transform.position.x, transform.position.y);
            if (transformData.position != currentPosition)
            {
                transformData.position = new Vector2(transform.position.x, transform.position.y);
            }

            if (transformData.rotateAngle != transform.eulerAngles.z)
            {
                transformData.rotateAngle = transform.eulerAngles.z;
            }
        });
    }
}
